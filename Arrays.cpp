#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

void printArray()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	for (int i = 0; i < arraySize; i++)
	{
		cout << items[i] << endl;
	}
}

void addArray()
{
	int numbers1[] = { 1, 2, 3, 4, 5 };
	int numbers2[] = { 0, 7, -10, 5, 3 };
	int arraySize = 5;
	int sums[5];

	// Compute
	for (int i = 0; i < arraySize; i++)
	{
		sums[i] = numbers1[i] + numbers2[i];
	}

	// Print result
	for (int i = 0; i < arraySize; i++)
	{
		cout << sums[i] << endl;
	}
}

void itemExists()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	// Get input
	string itemToFind;
	cout << "Input item to find: ";
	cin >> itemToFind;

	// Check if item exists
	bool itemExists = false;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemToFind == items[i])
		{
			itemExists = true;
			break;
		}
	}

	// Print result
	if (itemExists)
	{
		cout << itemToFind << " exists" << endl;
	}
	else
	{
		cout << itemToFind << " does not exist" << endl;
	}
}

void linearSearch()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	// Get input
	string itemToFind;
	cout << "Input item to find: ";
	cin >> itemToFind;

	// Check if item exists
	int itemAtIndex = -1;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemToFind == items[i])
		{
			itemAtIndex = i;
			break;
		}
	}

	// Print result
	if (itemAtIndex >= 0)
	{
		cout << itemToFind << " found at index " << itemAtIndex << endl;
	}
	else
	{
		cout << itemToFind << " does not exist" << endl;
	}
}

void checkInstancesOfItem()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	// Get input
	string itemToFind;
	cout << "Input item to find: ";
	cin >> itemToFind;

	// Count number of instances of the item
	int count = 0;
	for (int i = 0; i < arraySize; i++)
	{
		if (itemToFind == items[i])
		{
			count++;
		}
	}

	// Print result
	cout << "You have " << count << " " << itemToFind << endl;
}

void randomItem()
{
	string items[] = { "RedPotion", "BluePotion", "YggdrasilLeaf", "Elixir", "TeleportScroll", "RedPotion", "RedPotion", "Elixir" };
	int arraySize = 8;

	while (true)
	{
		system("cls");

		// Get random item
		int index = rand() % arraySize;
		string randomItem = items[index];
		cout << randomItem << endl;

		// Try to exit
		char exit;
		cout << "Enter 'x' to exit... ";
		cin >> exit;
		if (exit == 'x') break;
	}
}

void largest()
{
	int numbers[10] = { -20, 0, 7, 3, 2, 1, 8, -3, 6, 3 };

	// Get largest
	int largest = INT_MIN;
	for (int i = 0; i < 10; i++)
	{
		if (numbers[i] > largest)
		{
			largest = numbers[i];
		}
	}

	// Print result
	cout << "Largest number is " << largest << endl;
}

void selectionSort()
{
	int numbers[10];
	int n = 10;

	// Get input
	cout << "Enter 10 unsorted integers..." << endl;
	for (int i = 0; i < n; i++)
	{
		cout << "[" << i << "] = ";
		cin >> numbers[i];
	}

	// The last pass correctly sorts both x[n-2] and x[n-1] so we can skip it.
	for (int i = 0; i < n - 1; i++)
	{
		// Look for smallest number in the array starting from index i. 
		int p = i; // We'll initially set p to i to make it easier.
		for (int j = i + 1; j < n; j++) // We don't need to check for the first index in the subarray because we already initially set p as the first benchmark
		{
			// If x[i] is less than x[p], update p
			if (numbers[j] < numbers[p])
			{
				p = j;
			}
		}

		// Swap x[i] and x[p] since x[p] is the smallest number in the subarray x[i]...x[n-1]
		int temp = numbers[i];
		numbers[i] = numbers[p];
		numbers[p] = temp;
	}

	// Print the sorted array
	for (int i = 0; i < n; i++)
	{
		cout << numbers[i] << " ";
	}
	cout << endl;
}

// Efficient way of bubble sorting
void bubbleSort()
{
	int numbers[6] = { 12, 5, 21, 1, 15, 17 };
	int n = 6;

	// Initialize subArrayEnd
	int subArrayEnd = n - 1;

	// Loop until subArrayEnd reaches 0
	while (subArrayEnd > 0)
	{
		int nextEnd = 0;
		for (int j = 0; j < subArrayEnd; j++)
		{
			// Swap position of current index to the one next to it if it's smaller
			if (numbers[j] > numbers[j + 1])
			{
				int temp = numbers[j];
				numbers[j] = numbers[j + 1];
				numbers[j + 1] = temp;

				nextEnd = j;
			}
		}

		subArrayEnd = nextEnd;
	}

	// Print the sorted array
	for (int i = 0; i < n; i++)
	{
		cout << numbers[i] << " ";
	}
	cout << endl;
}

// A slight variation of the first version
void bubbleSort2()
{
	int numbers[6] = { 12, 5, 21, 1, 15, 17 };
	int n = 6;

	bool swapped = true;
	while (swapped)
	{
		swapped = false;
		for (int j = 0; j < n - 1; j++)
		{
			// Swap position of current index to the one next to it if it's smaller
			if (numbers[j] > numbers[j + 1])
			{
				int temp = numbers[j];
				numbers[j] = numbers[j + 1];
				numbers[j + 1] = temp;
				swapped = true;
			}
		}
	}

	// Print the sorted array
	for (int i = 0; i < n; i++)
	{
		cout << numbers[i] << " ";
	}
	cout << endl;
}

// A bit inefficient but code is a lot simpler and easier to remember
void bubbleSort3()
{
	int numbers[6] = { 12, 5, 21, 1, 15, 17 };
	int n = 6;

	// Sort
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n - 1; j++)
		{
			if (numbers[j] > numbers[j + 1])
			{
				int temp = numbers[j];
				numbers[j] = numbers[j + 1];
				numbers[j + 1] = temp;
			}
		}
	}

	// Print the sorted array
	for (int i = 0; i < n; i++)
	{
		cout << numbers[i] << " ";
	}
	cout << endl;
}

int main()
{
	srand(time(0));

	bubbleSort2();

	system("pause");
	return 0;
}
